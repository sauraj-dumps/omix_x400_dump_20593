#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from X400 device
$(call inherit-product, device/omix/X400/device.mk)

PRODUCT_DEVICE := X400
PRODUCT_NAME := lineage_X400
PRODUCT_BRAND := OMIX
PRODUCT_MODEL := X400
PRODUCT_MANUFACTURER := omix

PRODUCT_GMS_CLIENTID_BASE := android-omix

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="X400-user 11 R.X400.20221105.V11.4.2 323 release-keys"

BUILD_FINGERPRINT := OMIX/X400/X400:11/R.X400.20221105.V11.4.2/323:user/release-keys
